import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number: ");
        n = sc.nextInt();
        int i=0;
        while(i<n) {
            int j=1;
            while(j<=n) {
                System.out.print(j);
                j++;
            }
            System.out.println();
            i++;
        }
    }
}