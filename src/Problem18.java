import java.util.Scanner;

public class Problem18 {
    public static void main(String[] args) {
        int type;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please select star type [1-4,5 is Exit]: ");
        type = sc.nextInt();
        while (type>5) {
            System.out.println("Error: Please input number between 1-5");
            type = sc.nextInt();
            continue;
        }
            if (type==1) {
                int n;
                System.out.print("Please input number: ");
                n = sc.nextInt();
                for (int i=1; i<=n; i++) {
                    for (int j=0; j<i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (type==2) {
                int n;
                System.out.print("Please input number: ");
                n = sc.nextInt();
                for (int i=n; i>0; i--) {
                    for (int j=0; j<i; j++) {
                        System.out.print("*");
                    }
                    System.out.println();
                }
            }
            else if (type==3) {
                int n;
                System.out.print("Please input number: ");
                n = sc.nextInt();
                for (int i=0; i<n; i++) {
                    for (int j=0; j<5; j++) {
                        if(j>=i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }
            else if (type==4) {
                int n;
                System.out.print("Please input number: ");
                n = sc.nextInt();
                for (int i=n; i>=0; i--) {
                    for (int j=0; j<n; j++) {
                        if(j>=i) {
                            System.out.print("*");
                        } else {
                            System.out.print(" ");
                        }
                    }
                    System.out.println();
                }
            }
            else  {
                System.out.println("Bye bye!!!");
                sc.close();
        }
    }
}