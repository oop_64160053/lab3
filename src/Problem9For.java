import java.util.Scanner;

public class Problem9For {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number: ");
        n = sc.nextInt();
        for (int i=0; i<n; i++) {
            for (int j=1; j<=n; j++) {
                System.out.print(j);
            }
            System.out.println();
        }
    }
}